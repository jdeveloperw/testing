import com.google.common.base.CharMatcher;


public class CharMatcherTester implements MatcherTester {
    private final CharMatcher _matcher;

    public CharMatcherTester(CharMatcher matcher) {
        _matcher = matcher;
    }

    @Override
    public boolean matches(String input) {
        return _matcher.matchesAllOf(input);
    }
}
