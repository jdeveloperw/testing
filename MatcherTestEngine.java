import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import com.google.common.base.CharMatcher;

public class MatcherTestEngine {
    private static final Logger LOG = Logger.getAnonymousLogger();
    public static final int NUM_TESTS=1000000;

    public static void main(String[] args) {
        List<MatcherTester> matchers = new ArrayList<MatcherTester>();
        matchers.add(new CharMatcherTester(CharMatcher.JAVA_LETTER_OR_DIGIT.or(CharMatcher.is(':'))));
        matchers.add(new RegexTester("[\\p{Alnum}:]+"));
        matchers.add(new ScanTester());

        String[] validIds = generateRandomIds(NUM_TESTS, true);
        String[] invalidIds = generateRandomIds(NUM_TESTS, false);

        for (MatcherTester mt : matchers) {
            LOG.info(String.format("Matcher: %s, Num Runs: %d, Valid Strings: %b, Time (ms): %d",
                    mt.getClass().getName(), NUM_TESTS, true, test(mt, NUM_TESTS, validIds, true)));
            LOG.info(String.format("Matcher: %s, Num Runs: %d, Valid Strings: %b, Time (ms): %d",
                    mt.getClass().getName(), NUM_TESTS, false, test(mt, NUM_TESTS, invalidIds, false)));
        }
    }

    private static final String[] generateRandomIds(int number, boolean isValid) {
        String[] ids = new String[number];
        for (int i = 0 ; i < number ; i++) {
            ids[i] = isValid ? generateValidId() : generateInvalidId();
        }
        return ids;
    }

    private static String generateValidId() {
        return randomString().replaceAll("-", ":");
    }

    private static String generateInvalidId() {
        return randomString();
    }

    private static String randomString() {
        return UUID.randomUUID().toString();
    }

    private static final long test(MatcherTester mt, int numTests, String[] ids, boolean isValid) {
        long start = System.currentTimeMillis();
        for (int i = 0 ; i < numTests ; i++) {
            /*
             * Sanity check.
            boolean isMatch = mt.matches(ids[i]);
            assert isMatch  == isValid : String.format("Expected: %b, Actual: %b", isValid, isMatch);
            */
            mt.matches(ids[i]);
        }
        long end = System.currentTimeMillis();
        return end - start;
    }
}
