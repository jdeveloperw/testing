import java.util.regex.Pattern;

public class RegexTester implements MatcherTester {
    private final Pattern _pattern;

    public RegexTester(String pattern) {
        _pattern = Pattern.compile(pattern);
    }

    @Override
    public boolean matches(String input) {
        return _pattern.matcher(input).matches();
    }
}
