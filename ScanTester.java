public class ScanTester implements MatcherTester {
    @Override
    public boolean matches(String input) {
        for(int i=0; i<input.length(); i++) {
            char c = input.charAt(i);
            if( !Character.isLetterOrDigit(c) && c != ':') {
                return false;
            }
        }
        return true;
    }
}
