public interface MatcherTester {
    public boolean matches(String input);
}
